package br.com.loot.www.digitalbooklice.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import br.com.loot.www.digitalbooklice.models.Books;

@Dao
public interface BookDao {
    @Query("SELECT * FROM books")
    List<Books> getAllBooks();

    @Query("SELECT * FROM books WHERE id = :bookId LIMIT 1")
    Books getBook(Integer bookId);

    @Insert
    void InsertAll(List<Books> books);

    @Update
    void updateBook(Books book);
}
