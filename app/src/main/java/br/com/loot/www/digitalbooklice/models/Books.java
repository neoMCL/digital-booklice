package br.com.loot.www.digitalbooklice.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Books {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "isbn")
    private String isbn;

    @ColumnInfo(name = "price")
    private Integer price;

    @ColumnInfo(name = "currency_code")
    private String currencyCode;

    @ColumnInfo(name = "author")
    private String author;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "whishlist")
    private Boolean whishlist;

    @ColumnInfo(name = "favorite")
    private Boolean favorite;

    public Books(Integer id, String title, String isbn, Integer price, String currencyCode, String author, String description, Boolean whishlist, Boolean favorite) {
        this.id = id;
        this.title = title;
        this.isbn = isbn;
        this.price = price;
        this.currencyCode = currencyCode;
        this.author = author;
        this.description = description;
        this.whishlist = whishlist;
        this.favorite = favorite;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getWhishlist() {
        return whishlist;
    }

    public void setWhishlist(Boolean whishlist) {
        this.whishlist = whishlist;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }
}

