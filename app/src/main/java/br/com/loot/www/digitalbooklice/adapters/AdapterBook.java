package br.com.loot.www.digitalbooklice.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.loot.www.digitalbooklice.R;
import br.com.loot.www.digitalbooklice.models.Books;
import br.com.loot.www.digitalbooklice.utilities.CurrencyShow;

public class AdapterBook extends RecyclerView.Adapter<AdapterBook.BookViewHolder> implements Filterable {
    private List<Books> bookList;
    private List<Books> bookListFull;
    private Context context;
    private static final String DBNAME = "dbldatabase";


    public AdapterBook(List<Books> bookList, Context context) {
        this.bookList = bookList;
        this.bookListFull = new ArrayList<>(bookList);
        this.context = context;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_book_item, viewGroup, false);
        return new AdapterBook.BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder bookViewHolder, int i) {

        Books book = bookList.get(i);
        bookViewHolder.title.setText(book.getTitle());
        bookViewHolder.author.setText(book.getAuthor());
        bookViewHolder.price.setText(CurrencyShow.getCurrencyFormated(book.getCurrencyCode(), book.getPrice()));

    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }


    class BookViewHolder extends  RecyclerView.ViewHolder{
        TextView title;
        TextView price;
        TextView currencyCode;
        TextView author;
        ImageView cover;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.tvBookItemTitle);
            author = itemView.findViewById(R.id.tvBookItemAuthor);
            price = itemView.findViewById(R.id.tvBookItemPrice);
        }
    }


    @Override
    public Filter getFilter() {
        return bookFilter;
    }

    private Filter bookFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Books> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0){
                filteredList.addAll(bookListFull);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Books item : bookListFull ){
                    if (item.getTitle().toLowerCase().contains(filterPattern)){
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            bookList.clear();
            bookList.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };
}
