package br.com.loot.www.digitalbooklice.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import br.com.loot.www.digitalbooklice.dao.BookDao;
import br.com.loot.www.digitalbooklice.models.Books;

@Database(entities = {Books.class}, version = 1, exportSchema = false)
public abstract class BooksDatabase extends RoomDatabase {

    public abstract BookDao getBookDao();
}
