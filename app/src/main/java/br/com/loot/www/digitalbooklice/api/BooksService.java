package br.com.loot.www.digitalbooklice.api;

import br.com.loot.www.digitalbooklice.models.Books;
import retrofit2.http.GET;
import retrofit2.Call;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;


public interface BooksService {

    @GET("books")
    Call<List<Books>> getListBook();

    @GET("book/{id}")
    Call<Books> getBook(@Path("id") int id);

}
