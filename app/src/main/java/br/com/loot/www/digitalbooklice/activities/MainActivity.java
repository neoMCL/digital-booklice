package br.com.loot.www.digitalbooklice.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import java.util.ArrayList;
import java.util.List;
import br.com.loot.www.digitalbooklice.R;
import br.com.loot.www.digitalbooklice.adapters.AdapterBook;
import br.com.loot.www.digitalbooklice.dao.BookDao;
import br.com.loot.www.digitalbooklice.database.BooksDatabase;
import br.com.loot.www.digitalbooklice.database.GeneratorDB;
import br.com.loot.www.digitalbooklice.listeners.RecyclerItemClickListener;
import br.com.loot.www.digitalbooklice.models.Books;
import retrofit2.Retrofit;


public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Books> bookList = new ArrayList<>();
    private AdapterBook adapterBook;
    private Retrofit retrofit;
    private BookDao bookDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rvBookList);

        initDB();
        configRecyclerView();
        configRecyclerViewClickListener();
    }

    private void initDB() {
        GeneratorDB generatorDB = new GeneratorDB();
        BooksDatabase database = generatorDB.genDB(getApplicationContext());
        BookDao bookDao = database.getBookDao();
        bookList = bookDao.getAllBooks();
    }


    private void configRecyclerViewClickListener() {
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(
                this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Books book = bookList.get(position);
                int bookID = book.getId();
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("bookID", bookID);
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        }
        ));
    }


    public void configRecyclerView(){
        adapterBook = new AdapterBook(bookList, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter( adapterBook );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapterBook.getFilter().filter(s);
                return false;
            }
        });
        return true;
    }
}