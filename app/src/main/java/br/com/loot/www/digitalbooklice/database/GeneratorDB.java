package br.com.loot.www.digitalbooklice.database;

import android.arch.persistence.room.Room;
import android.content.Context;

public class GeneratorDB {

    public BooksDatabase genDB(Context context){
        final String BOOKDB = "dbsdb";

        return Room.databaseBuilder(context, BooksDatabase.class, BOOKDB).allowMainThreadQueries().build();
    }
}
