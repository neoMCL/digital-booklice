package br.com.loot.www.digitalbooklice.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import java.util.List;
import br.com.loot.www.digitalbooklice.R;
import br.com.loot.www.digitalbooklice.api.BooksService;
import br.com.loot.www.digitalbooklice.api.RetrofitConfig;
import br.com.loot.www.digitalbooklice.dao.BookDao;
import br.com.loot.www.digitalbooklice.database.GeneratorDB;
import br.com.loot.www.digitalbooklice.models.Books;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class SplashScreenActivity extends AppCompatActivity {
    private BookDao bookDao;
    TextView tvLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        tvLoading = findViewById(R.id.tvSplashLoading);

        GeneratorDB generatorDB = new GeneratorDB();
        bookDao = generatorDB.genDB(getApplicationContext()).getBookDao();

        List<Books> list = bookDao.getAllBooks();


        if (isNetworkAvailable()){
            tvLoading.setText("Loading...");

            if(list.isEmpty()) {
                getBookList();
            }else{
                goToMainActivity();
            }
        }else{
            if(!list.isEmpty()) {
                goToMainActivity();
            }else{
                tvLoading.setText("No internet connection :( ");
            }
        }
    }


    private void goToMainActivity() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }


    private void getBookList() {
        Retrofit retrofit = new RetrofitConfig().getRetrofit();
        BooksService booksService = retrofit.create(BooksService.class);
        booksService.getListBook().enqueue(new Callback<List<Books>>() {

            @Override
            public void onResponse(Call<List<Books>> call, Response<List<Books>> response) {

                if (response.isSuccessful()) {
                    bookDao.InsertAll(response.body());
                    goToMainActivity();
                }
            }

            @Override
            public void onFailure(Call<List<Books>> call, Throwable t) {

            }
        });
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}