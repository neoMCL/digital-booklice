package br.com.loot.www.digitalbooklice.utilities;

import java.text.NumberFormat;
import java.util.Currency;

public class CurrencyShow {

    public static String getCurrencyFormated(String currencyCode, int amountInt){
        double amount = amountInt;
        amount = amount / 100;
        String cd = currencyCode;
        Currency currency = Currency.getInstance(cd);
        NumberFormat format = NumberFormat.getCurrencyInstance();
        format.setMaximumFractionDigits(2);
        format.setCurrency(currency);

        return format.format(amount);
    }
}
