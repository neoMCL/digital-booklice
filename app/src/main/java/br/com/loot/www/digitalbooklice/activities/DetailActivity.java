package br.com.loot.www.digitalbooklice.activities;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import br.com.loot.www.digitalbooklice.R;
import br.com.loot.www.digitalbooklice.api.BooksService;
import br.com.loot.www.digitalbooklice.api.RetrofitConfig;
import br.com.loot.www.digitalbooklice.dao.BookDao;
import br.com.loot.www.digitalbooklice.database.BooksDatabase;
import br.com.loot.www.digitalbooklice.database.GeneratorDB;
import br.com.loot.www.digitalbooklice.models.Books;
import br.com.loot.www.digitalbooklice.utilities.CurrencyShow;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class DetailActivity extends AppCompatActivity {

    private int bookID;
    private TextView tvDetailDescription, tvDetailAuthor, tvDetailTitle, tvNotFound;
    private ImageView ivNotFound, ivFavorite, ivShare, ivWhisilist;
    private Button buttonBuy;
    private ConstraintLayout clDetailBook;
    private ProgressBar progressBarDetailBook;
    private BookDao bookDao;
    Books book;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        bindComponents();
        getBookId();
        initDB();
        setFavoriteIcon();
        setWishlistIcon();
        setUpListeners();

        if (book.getDescription() == null){
            getBook();
        }else{
            fillFields();
        }
    }


    private void setUpListeners() {
        ivFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favorite();
            }
        });

        ivWhisilist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wishlist();
            }
        });
    }


    private void initDB() {
        GeneratorDB generatorDB = new GeneratorDB();
        BooksDatabase database = generatorDB.genDB(getApplicationContext());
        bookDao = database.getBookDao();
        book = bookDao.getBook(bookID);
    }


    private void getBookId() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            bookID = bundle.getInt("bookID");
        }
    }


    private void bindComponents() {
        clDetailBook = findViewById(R.id.clDetailBook);
        progressBarDetailBook = findViewById(R.id.progressBarDetailBook);
        tvDetailDescription = findViewById(R.id.tvDetailDescription);
        tvDetailAuthor = findViewById(R.id.tvDetailAuthor);
        tvDetailTitle = findViewById(R.id.tvDetailTitle);
        buttonBuy = findViewById(R.id.buttonDetailBuy);
        tvNotFound = findViewById(R.id.tvNotFound);
        ivNotFound = findViewById(R.id.ivNotFound);
        ivFavorite = findViewById(R.id.buttonFavorite);
        ivShare = findViewById(R.id.buttonShare);
        ivWhisilist = findViewById(R.id.buttonWishlist);
    }


    private void getBook() {
        Retrofit retrofit = new RetrofitConfig().getRetrofit();
        BooksService booksService = retrofit.create(BooksService.class);
        booksService.getBook(bookID).enqueue(new Callback<Books>() {
            @Override
            public void onResponse(Call<Books> call, Response<Books> response) {

                if (response.isSuccessful()) {
                    book = response.body();
                    bookDao.updateBook(book);
                    fillFields();
                }else{
                    progressBarDetailBook.setVisibility(View.GONE);
                    tvNotFound.setVisibility(View.VISIBLE);
                    ivNotFound.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Books> call, Throwable t) {
            }
        });
    }


    public void fillFields(){
        String description = book.getDescription().replace(". ",".\n\n");
        tvDetailDescription.setText(description);
        tvDetailAuthor.setText(book.getAuthor());
        tvDetailTitle.setText(book.getTitle());
        String buttonCaption = "Buy now " + CurrencyShow.getCurrencyFormated(book.getCurrencyCode(), book.getPrice());
        buttonBuy.setText(buttonCaption);
        progressBarDetailBook.setVisibility(View.GONE);
        clDetailBook.setVisibility(View.VISIBLE);
    }


    public void setFavoriteIcon(){
        if (book.getFavorite() == null || book.getFavorite() == false){
            ivFavorite.setImageResource(R.drawable.ic_favorite_border_red_24dp);
        }else{
            ivFavorite.setImageResource(R.drawable.ic_favorite_full_red_24dp);
        }
    }


    public void favorite(){
        if (book.getFavorite() == null || book.getFavorite() == false){
            book.setFavorite(true);
            bookDao.updateBook(book);
            setFavoriteIcon();
        }else{
            book.setFavorite(false);
            bookDao.updateBook(book);
            setFavoriteIcon();
        }
    }


    public void setWishlistIcon(){
        if (book.getWhishlist() == null || book.getWhishlist() == false){
            ivWhisilist.setImageResource(R.drawable.ic_bookmark_border_black_24dp);
        }else{
            ivWhisilist.setImageResource(R.drawable.ic_bookmark_full_black_24dp);
        }
    }


    public void wishlist(){
        if (book.getWhishlist() == null || book.getWhishlist() == false){
            book.setWhishlist(true);
            bookDao.updateBook(book);
            setWishlistIcon();
        }else{
            book.setWhishlist(false);
            bookDao.updateBook(book);
            setWishlistIcon();
        }
    }

}